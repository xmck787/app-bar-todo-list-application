# app-bar-todo-list-application



## API


**1. Create Task**
```
POST http://localhost:3000/tasks/
```
Request:
```json
{
    "name": "Task 4",
    "description": "Task 4",
    "dueDate": "2021-09-10",
    "status": "Normal"
}
```

**2. Get All Tasks (Order by)**
```
GET http://localhost:3000/tasks/
```
Request(Optional):
```json
[
    {
        "_id": "612b36f0b5eb23b1ae92e6ee",
        "name": "Task 3",
        "description": "Task 3",
        "dueDate": "2021-09-10T00:00:00.000Z",
        "status": "Normal",
        "__v": 0
    },
    {
        "_id": "612b1ace8eb83f44060786e7",
        "name": "Task 2",
        "description": "Task 2",
        "dueDate": "2021-09-25T00:00:00.000Z",
        "status": "Normal",
        "__v": 0
    },
    {
        "_id": "612b208b0899c8487b4ddc1a",
        "name": "Task todo",
        "description": "Task todo",
        "dueDate": "2021-09-25T00:00:00.000Z",
        "status": "Completed",
        "__v": 0
    }
]
```

**3. Get Status Tasks (Order by)**
```
GET http://localhost:3000/tasks/Normal
GET http://localhost:3000/tasks/Completed
```
Request(Optional):
```json
{
    "orderBy": "name", --dueDate
    "sortBy": "desc" --asc
}
```
Response:
```json
[
    {
        "_id": "612b36f0b5eb23b1ae92e6ee",
        "name": "Task 3",
        "description": "Task 3",
        "dueDate": "2021-09-10T00:00:00.000Z",
        "status": "Normal",
        "__v": 0
    },
    {
        "_id": "612b1ace8eb83f44060786e7",
        "name": "Task 2",
        "description": "Task 2",
        "dueDate": "2021-09-25T00:00:00.000Z",
        "status": "Normal",
        "__v": 0
    }
]
```

**4. Update Tasks**
```
PUT http://localhost:3000/tasks/<taskId>
PUT http://localhost:3000/tasks/612b208b0899c8487b4ddc1a
```
Request:
```json
{
    "name":"Task todo 1",
    "description":"Task todo 1",
    "dueDate":"2021-09-25",
    "status":"Completed"
}
```
Response:
```json
{
    "_id": "612b208b0899c8487b4ddc1a",
    "name": "Task todo",
    "description": "Task todo",
    "dueDate": "2021-09-25T00:00:00.000Z",
    "status": "Completed",
    "__v": 0
}
```

**5. Update Task Status**
```
PATCH http://localhost:3000/tasks/<taskId>
PATCH http://localhost:3000/tasks/612b208b0899c8487b4ddc1a
```
Request:
```json
{
    "status":"Normal"
}
```
Response:
```json
{
    "acknowledged": true,
    "modifiedCount": 1,
    "upsertedId": null,
    "upsertedCount": 0,
    "matchedCount": 1
}
```

**6. Delete Task**
```
DELETE http://localhost:3000/tasks/<taskId>
DELETE http://localhost:3000/tasks/612b208b0899c8487b4ddc1a
```
Response:
```json
{
    "deletedCount": 1
}
```
