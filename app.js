const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();
require('dotenv/config');

app.use(bodyParser.json());

// import routes
const taskRoute = require('./routes/route_tasks');
app.use('/tasks', taskRoute);

// connected to db
mongoose.connect(
    process.env.DB_CONNECTION, () => {
    console.log('connected to db')
    });

// port
app.listen(3000);