const express = require('express');
const router = express.Router();
const Task = require('../model/Task');

const statusArray = ['Normal', 'Completed'];

// Create - submit task
router.post('/', async (req, res) => {
    console.log(req.body);

    try {
        // check status
        if (!statusArray.includes(req.body.status)) {
            res.json({ message: 'Status should be - ' + statusArray });
            return;
        }

        // convert string to date
        const dueDate = new Date(req.body.dueDate);

        const task = new Task({
            name: req.body.name,
            description: req.body.description,
            dueDate: dueDate,
            status: req.body.status
        });

        const saveTask = await task.save();
        res.json(saveTask);
    } catch (err) {
        res.json({ message: err });
    }
});

// Read - get all tasks
router.get('/', async (req, res) => {
    var orderBy = 'asc';

    if ('desc' == req.body.orderBy) {
        orderBy = 'desc';
    }

    try {

        if ('name' == req.body.sortBy) {
            const tasks = await Task.find().sort({ name: orderBy });
            res.json(tasks);
        } else {
            const tasks = await Task.find().sort({ dueDate: orderBy });
            res.json(tasks);
        }
    } catch (err) {
        res.json({ message: err });
    }
});

// Read - specific task
router.get('/:status', async (req, res) => {
    console.log(req.params.status);

    var orderBy = 'asc';

    if ('desc' == req.body.orderBy) {
        orderBy = 'desc';
    }

    try {

        if ('name' == req.body.sortBy) {
            const tasks = await Task.find({
                status: req.params.status
            }).sort({ name: orderBy });
            res.json(tasks);
        } else {
            const tasks = await Task.find({
                status: req.params.status
            }).sort({ dueDate: orderBy });
            res.json(tasks);
        }
    } catch (err) {
        res.json({ message: err });
    }
});

// Update - update task 
router.put('/:taskId', async (req, res) => {
    console.log(req.params.taskId);

    try {
        // check status
        if (!statusArray.includes(req.body.status)) {
            res.json({ message: 'Status should be - ' + statusArray });
            return;
        }

        // convert string to date
        const dueDate = new Date(req.body.dueDate);

        const updateTasksId = await Task.findOneAndUpdate({
            _id: req.params.taskId
        }, {
            $set: {
                name: req.body.name,
                description: req.body.description,
                dueDate: dueDate,
                status: req.body.status
            }
        });
        res.json(updateTasksId);
    } catch (err) {
        res.json({ message: err });
    }
});

// Update - update task status
router.patch('/:taskId', async (req, res) => {
    console.log(req.params.taskId);

    try {
        // check status
        if (!statusArray.includes(req.body.status)) {
            res.json({ message: 'Status should be - ' + statusArray });
            return;
        }

        const updateTasksId = await Task.updateOne({
            _id: req.params.taskId
        }, {
            $set: {
                status: req.body.status
            }
        });
        res.json(updateTasksId);
    } catch (err) {
        res.json({ message: err });
    }
});

// Delete - delete task
router.delete('/:taskId', async (req, res) => {
    console.log(req.params.taskId);

    try {
        const removeTasks = await Task.remove({
            _id: req.params.taskId
        });
        res.json(removeTasks);
    } catch (err) {
        res.json({ message: err });
    }
});


module.exports = router;