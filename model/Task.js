const mongoose = require('mongoose');
const TaskSchema = mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: true
    },
    dueDate: Date,
    status: {
        type: String,
        require: true
    }
});

module.exports = mongoose.model('Task', TaskSchema);